import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RemoveDuplicates {
    public static void main(String args[]){
        List<Integer> integers = Arrays.asList(1,4,5,3,2,4);
        Stream<Integer> s1=integers.stream().distinct();
        System.out.println(s1.collect(Collectors.toList()));
        System.out.println(integers.stream().distinct().map(i->i*i).collect(Collectors.toList()));
    }
}
