import java.util.Scanner;

class FibonacciEx{
    public static void main(String args[]) {
        int number, n1 = 0,n2 = 1, n3;
        System.out.println("Enter the number that you want to calculate the fibo series till:");
        number = new Scanner(System.in).nextInt();

            for (int i = 0; i < number; i++) {
                n3 = n1 + n2;
                System.out.print(n3+" ");
                n1 = n2;
                n2 = n3;
            }

    }

}

