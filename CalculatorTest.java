import java.util.Scanner;

public class CalculatorTest {
    public static void main(String args[]){
        System.out.println("Enter the numbers:");
        double number1 = new Scanner(System.in).nextDouble();
        double number2 = new Scanner(System.in).nextDouble();
        System.out.println("Enter the operation that is to be performed");
        String operation = new Scanner(System.in).nextLine();
        switch(operation){
            case "Add":
                System.out.println("Addition of the numbers is:"+ Calculator.add(number1,number2));
                break;
            case "Subtract":
                System.out.println("Subtraction of the numbers is:" +Calculator.subtract(number1,number2));
                break;
            case "Multiply":
                System.out.println("Multiplication of the numbers is:" +Calculator.multiply(number1,number2));
                break;
            case "Divide":
                System.out.println("Division of the numbers is:" +Calculator.divide(number1,number2));
                break;

                default:
                System.out.println("Enter a valid operation:");
                break;
        }
    }
}
