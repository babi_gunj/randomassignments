import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StringToUpperCase {
    public static void main(String args[]){
        List<String> strings = Arrays.asList("usa","canada","uk","asia","france");
        System.out.println( strings.stream().map(i->i.toUpperCase()).collect(Collectors.toList()));
        System.out.println( strings.stream().map(i->i.toUpperCase()).collect(Collectors.joining(",")));

    }
}
