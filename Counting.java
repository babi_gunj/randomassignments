import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Counting {
    public static void main(String args[]){
        List<Integer> list = Arrays.asList(1,2,2,4,2,5);
        System.out.println(list.stream().filter(i->i-2==0).collect(Collectors.counting()));
        System.out.println(list.stream().filter(i->i==2).count());
    }
}
