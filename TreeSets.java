import java.util.TreeSet;

public class TreeSets {
    public static void main(String args[]){
        TreeSet<String> te = new TreeSet<String>();
        te.add("same");
        te.add("hello");
        te.add("Same");
        te.add("Super");
        System.out.println(te);
        te.remove("same");
        System.out.println(te);
    }
}
