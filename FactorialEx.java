import java.util.Scanner;

public class FactorialEx {
    public static void main(String args[]){
        int factorial=1;
        System.out.println("Enter the number whose factorial is to be found:");
        int number = new Scanner(System.in).nextInt();
        for(int i=1;i<=number;i++)
            factorial = factorial * i;
        System.out.println("The factorial is:"+factorial);
    }
}
