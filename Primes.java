import java.util.Scanner;

public class Primes {
    public static void main(String args[]){
        int number,m,flag=0;
        System.out.println("Enter the number:");
        number = new Scanner(System.in).nextInt();
        m=number/2;
        if(number==0||number==1){
            System.out.println(number+" is not prime number");
        }else{
            for(int i=2;i<=m;i++){
                if(number%i==0){
                    System.out.println(number+" is not prime number");
                    flag=1;
                    break;
                }
            }
            if(flag==0)  { System.out.println(number+" is prime number"); }
        }//end of else
    }
}


