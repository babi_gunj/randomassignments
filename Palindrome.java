import java.util.Scanner;

public class Palindrome {
    public static void main(String args[]){
        String reverse="";
        String number=null;
        System.out.println("Enter the number:");
        number=new Scanner(System.in).next();
        for(int i=0;i<number.length();i++){
            reverse = reverse +number.charAt(i);
        }
        if(reverse.equals(number))
            System.out.println("Palindrome");
        else
        System.out.println("Not Palindrome");
    }
}
