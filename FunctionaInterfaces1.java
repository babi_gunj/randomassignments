import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FunctionaInterfaces1 {
    public static void main(String args[]){
        List<String> strings = Arrays.asList("abc","bcd","defg","jk","abc");
        System.out.println(strings.stream().filter(i->i.length()>2).collect(Collectors.toList()));
    }
}
