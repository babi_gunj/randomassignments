import java.util.Iterator;
import java.util.Vector;

public class VectorEx {

    public static void main(String args[]){
        Vector v = new Vector();
        v.add(1);
        v.add("Hello");
        v.add(1,"babe");
        System.out.println(v);
        Iterator i = v.iterator();
        while(i.hasNext()){
            System.out.println(i.next());
        }
    }


}
